/*

	project:	SleepTracker
	what:		Application startup script
	started:	March 2013
	who:		Frode Klevstul (frode at klevstul dot com)

	-- REFERENCES --

	Ext JS	: http://www.sencha.com/products/extjs/
    API		: http://docs.sencha.com/ext-js/4-2/

*/

/*
Ext.application({
    name: 'HelloExt',
    launch: function() {
        Ext.create('Ext.container.Viewport', {
            layout: 'fit',
            items: [
                {
                    title: 'Hello, Ext',
                    html : 'Hello! Welcome to Ext JS.'
                }
            ]
        });
    }
});
*/

//Ext.Loader.setConfig({enabled:true});

Ext.application({
	name: 'SleepTracker',
	appFolder: 'app',
	autoCreateViewport: true,
	models: ['SleepRecord', 'SleepRecordDay'],
	stores: ['SleepRecords', 'SleepRecordDays'],
	launch: function() {
		console.log('Application()');
	}
});

