/*
	what:		Global objects to store key data
	started:	March 2013
	who:		Frode Klevstul (frode at klevstul dot com)
*/
Ext.namespace('Global');

Global.session = new Object;
Global.session.username = '';
Global.session.password = '';
Global.session.requestId = '';

Global.parameters						= new Object;
Global.parameters.applicationName		= 'SleepTracker';
Global.parameters.applicationVersion	= '0.1.1304';
Global.parameters.credits				= 'Frode Klevstul (<a href="http://www.watashi.no" target="_blank">www.watashi.no</a>)';
Global.parameters.jsonBasePath			= 'http://urubu.tele2.no/cgi/';
Global.parameters.rpcMethod				= 'jsonRpc.py';
Global.parameters.rpcMethodFull			= Global.parameters.jsonBasePath + Global.parameters.rpcMethod;
Global.parameters.moduleParam			= 'p_module';
Global.parameters.actionParam			= 'p_action';
