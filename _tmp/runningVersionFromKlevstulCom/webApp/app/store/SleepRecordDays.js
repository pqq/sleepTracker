/*
	what:		Store for the sleep record day grid
	started:	April 2013
	who:		Frode Klevstul (frode at klevstul dot com)
*/

Ext.define('SleepTracker.store.SleepRecordDays', {
    extend: 'Ext.data.Store',
    model: 'SleepTracker.model.SleepRecordDay',
	//pageSize: 5, // to be changed when paging is fully implemented
	// the following data section is to be removed and replaced with an ajax call in the model that fetches data from a given url
	data:{'results':[
        {
            'pk': 1,
            'timestamp_sleep': '10.04.2013',
			'minutes': 432
        },
        {
            'pk': 2,
            'timestamp_sleep': '11.04.2013',
			'minutes': 543
		},
        {
            'pk': 3,
            'timestamp_sleep': '12.04.2013',
			'minutes': 572
        },
        {
            'pk': 4,
            'timestamp_sleep': '13.04.2013',
			'minutes': 492
        },
        {
            'pk': 5,
            'timestamp_sleep': '14.04.2013',
			'minutes': 549
        },
        {
            'pk': 6,
            'timestamp_sleep': '15.04.2013',
			'minutes': 534
        },
        {
            'pk': 7,
            'timestamp_sleep': '16.04.2013',
			'minutes': 510
        },
        {
            'pk': 8,
            'timestamp_sleep': '17.04.2013',
			'minutes': 572
        },
        {
            'pk': 9,
            'timestamp_sleep': '18.04.2013',
			'minutes': 434
        },
        {
            'pk': 10,
            'timestamp_sleep': '19.04.2013',
			'minutes': 510
        },
        {
            'pk': 11,
            'timestamp_sleep': '20.04.2013',
			'minutes': 564
        },
        {
            'pk': 12,
            'timestamp_sleep': '21.04.2013',
			'minutes': 490
        },
        {
            'pk': 13,
            'timestamp_sleep': '22.04.2013',
			'minutes': 513
        },
        {
            'pk': 14,
            'timestamp_sleep': '23.04.2013',
			'minutes': 521
        },
        {
            'pk': 15,
            'timestamp_sleep': '24.04.2013',
			'minutes': 501
        },
        {
            'pk': 16,
            'timestamp_sleep': '25.04.2013',
			'minutes': 578
        },
        {
            'pk': 17,
            'timestamp_sleep': '26.04.2013',
			'minutes': 509
        },
        {
            'pk': 18,
            'timestamp_sleep': '27.04.2013',
			'minutes': 522
        },
        {
            'pk': 19,
            'timestamp_sleep': '28.04.2013',
			'minutes': 531
        },
        {
            'pk': 20,
            'timestamp_sleep': '29.04.2013',
			'minutes': 529
        },
        {
            'pk': 21,
            'timestamp_sleep': '30.04.2013',
			'minutes': 612
        },
        {
            'pk': 22,
            'timestamp_sleep': '31.04.2013',
			'minutes': 313
        },
        {
            'pk': 23,
            'timestamp_sleep': '01.05.2013',
			'minutes': 487
        },
        {
            'pk': 24,
            'timestamp_sleep': '02.05.2013',
			'minutes': 500
        },
        {
            'pk': 25,
            'timestamp_sleep': '03.05.2013',
			'minutes': 508
        },
        {
            'pk': 26,
            'timestamp_sleep': '04.05.2013',
			'minutes': 534
        },
        {
            'pk': 27,
            'timestamp_sleep': '05.05.2013',
			'minutes': 593
        },
        {
            'pk': 28,
            'timestamp_sleep': '06.05.2013',
			'minutes': 180
        }
    ]}
});
