/*
	what:		A panel for presenting different grids
	started:	April 2013
	who:		Frode Klevstul (frode at klevstul dot com)
*/

Ext.define('SleepTracker.view.GridMainView', {
	extend:		'Ext.TabPanel',
	alias:		'widget.gridmainview',
	activeTab:	0,
	//width:		600,
	//height:		250,
	plain:		true,
	defaults :{
		autoScroll: true,
		bodyPadding: 10
	},
	//html: '[trend] [length] [pattern] [sleep time] [wake time] [stats]'
	items: [{
			title: 'Sleep records - day',
			items: {xtype: 'sleeprecorddaygrid'},
			//html: 'Show trend chart',
			//items: {xtype: 'trendview'}
			//html: 'show all records grouped into days'
			listeners: {
				activate: function(tab) {
					var trendView = Ext.ComponentQuery.query('trendview')[0];
					var store = Ext.getStore('SleepRecordDays');
					trendView.setStore(store, 'sleeprecorddaygrid');
				}
			}


		},{
			title: 'Sleep records - entries',
			//html: 'tst',
			items: {xtype: 'sleeprecordgrid'},
			listeners: {
				activate: function(tab) {
					var trendView = Ext.ComponentQuery.query('trendview')[0];
					var store = Ext.getStore('SleepRecords');
					trendView.setStore(store, 'sleeprecordgrid');
				}
			}

			//listeners: {
			//	activate: function(tab) {
			//		//tab.loader.load();
			//		console.log('do something...');
			//	}
			//}
		}
	]
,
	initComponent: function() {
		console.log('GridMainView()');
		this.callParent();
	}

});