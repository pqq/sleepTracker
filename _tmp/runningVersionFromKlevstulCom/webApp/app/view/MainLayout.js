/*

	what:		Main layout for the application
	started:	March 2013
	who:		Frode Klevstul (frode at klevstul dot com)

	-- LAYOUT --

    |-------------------------------------------------|
    |                  Sleep Tracker                  |
    |-------------------------------------------------|
    | |---------------------------------------------| |
    | |             [date controller]               | |
    | |---------------------------------------------| |
    | |---------------| |---------------------------| |
    | | sleep records | |     presentation view     | |
    | |...............| |---------------------------| |
    | |...............| |  [presentation selector]  | |
    | |...............| | |-----------------------| | |
    | |...............| | |   data presentation   | | |
    | |...............| | |.......................| | |
    | |...............| | |.......................| | |
    | |...............| | |.......................| | |
    | |...............| | |.......................| | |
    |-------------------------------------------------|

*/

Ext.define('SleepTracker.view.MainLayout', {
    extend: 'Ext.panel.Panel',
	alias: 'widget.mainlayout',
	itemId:	'mainlayout',
	title: Global.parameters.applicationName,
	layout: 'border',
bodyStyle: 'padding:5px;background:#eee;',
//bodyBorder: true,
//border: true,
defaults: {
    collapsible: true,
    split: true,
    bodyPadding: 5
}
,
items: [
{
    title: 'Date selector',
    region: 'north',
    height: 100,
    minHeight: 75,
    maxHeight: 250,
    //html: 'fields for selecting dates will come here'
	items: {
		xtype: 'datecontroller'
	}
}
,
{
//    title: 'Sleep records',
    collapsible: false,
	itemId: 'mainlayout_west',
    region:'west',
    //floatable: true,
    //margins: '5 0 0 0',
    width: 850,
    //minWidth: 100,
    //maxWidth: 500,
    //html: 'Sleep records will show up here'
	items: {
		xtype: 'gridmainview'
		//xtype: 'sleeprecordgrid'
	}
}
,
{
    //title: 'Presentation view',
    collapsible: false,
    region: 'center',
    //html: 'Presentation of charts etc will go here'
	items: [
		{
			//title:	'Presentation view',
			region:	'north',
		    //margins: '0 0 50 0',
			//bodyStyle: 'padding:10px;',
			//html: '[trend] [length] [pattern] [sleep time] [wake time] [stats]'
			xtype: 'presentationview'

		}
/*		,
		{
			title:	'the data',
			region:	'center',
			html:	'data presentation comes here'
		}
*/
	]
}
,
{
	//MuddyGlobal.parameters.applicationName.toUpperCase()
    title: 'v ' + Global.parameters.applicationVersion,
    region: 'south',
    height: 60,
	collapsed: true,
    //minHeight: 50,
    //maxHeight: 100,
    html: Global.parameters.applicationName + ' // by ' + Global.parameters.credits
	
}

]

/*
		{
			region: 'center',
			layout: {
				type: 'table',
				align: 'center'
			},
			autoScroll: true,
			items: [
                {
                    title: 'Hello, Ext',
                    html : 'Hello! Welcome to Ext JS.',
					frame: true
                }


				{
					xtype: 'panel',
//					margins: {top:10, right:10, bottom:10, left:10},
					layout: 'auto',
					title: 'DateSelector',
//					height: 80,
//					width: 1400,
//					flex: 0,
					items: {
//						xtype: 'filtercontrols'
						title: 'Hello, Ext',
						html : 'Hello! Welcome to Ext JS.',
						//frame: true

					}

				}


				,
				{
					xtype: 'panel',
					itemId: 'panel-commissionrules',
					width: 1400,
					title: 'Commission rules',
					items: {
						xtype: 'commissionlist'
					}
				}
			]
		}
*/
/*		,
		{
			xtype: 'commissionruledetails',
			region: 'east',
			collapsible: true,
			title: 'Commission details'
		}

	]
*/
,
	initComponent: function() {
		console.log('MainLayout()');
		this.callParent();
	}

});