/*
	what:		The presentation view, for a (visual) presentation of the data
	started:	April 2013
	who:		Frode Klevstul (frode at klevstul dot com)
*/

Ext.define('SleepTracker.view.PresentationView', {
	extend:		'Ext.TabPanel',
	alias:		'widget.presentationview',
	activeTab:	0,
	//width:		600,
	//height:		250,
	plain:		true,
	defaults :{
		autoScroll: true,
		bodyPadding: 10
	},
	//html: '[trend] [length] [pattern] [sleep time] [wake time] [stats]'
	items: [{
			title: 'Trend',
			//html: 'Show trend chart',
			items: {xtype: 'trendview'}
/*			listeners: {
				activate: function(tab) {
					var trendView = this.getComponent('trendview');
					trendView.drawchart();					
				}
			}
*/

		},{
			title: 'Length',
			disabled: true,
			loader: {
				url: 'ajax1.htm',
				contentType: 'html',
				loadMask: true
			},
			listeners: {
				activate: function(tab) {
					//tab.loader.load();
					console.log('tab clicked...');
				}
			}
		},{
			title: 'Pattern',
			disabled: true,
			loader: {
				url: 'ajax2.htm',
				contentType: 'html',
				//autoLoad: true,
				params: 'foo=123&bar=abc'
			},
			listeners: {
				activate: function(tab) {
					//tab.loader.load();
					console.log('tab clicked...');
				}
			}
		},{
			title: 'Sleep time',
			disabled: true,
			listeners: {
				activate: function(tab){
					setTimeout(function() {
						console.log(tab.title + ' was activated.');
					}, 1);
				}
			},
			html: "I am tab 4's content. I also have an event listener attached."
		},{
			title: 'Wake time',
			disabled: true,
			html: 'wake time chart'
		},{
			title: 'Stat',
			//html: 'show statistics'
			items: {xtype: 'statisticsview'},
			listeners: {
				activate: function(tab) {
					var statisticsView = this.getComponent('statisticsview');
					statisticsView.loadstats();					
				}
			}
		}
	]
,
	initComponent: function() {
		console.log('PresentationView()');
		this.callParent();
	}

});