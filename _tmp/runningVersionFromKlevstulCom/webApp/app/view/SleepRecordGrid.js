/*
	what:		Present all sleep records in a grid
	started:	March 2013
	who:		Frode Klevstul (frode at klevstul dot com)
*/

Ext.define('SleepTracker.view.SleepRecordGrid', {
    extend: 'Ext.grid.Panel',
	itemId: 'sleeprecordgrid',
	alias: 'widget.sleeprecordgrid',
	//stateId: 'commissionListGrid',
	viewConfig: {
		stripeRows: true
	},
	store: 'SleepRecords',
	border: false,
	layout: 'fit',
	//autoScroll: true,
//width: 500,
	//height: 600,
	//overflowX: false,
	//overflowY: 'auto',
//	dockedItems: [{
//		xtype: 'pagingtoolbar',
//		store: 'CommissionRules',
//		dock: 'bottom',
//		displayInfo: true
//	}],

plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 2
        })
    ],
	columns: [
		{
			text     : 'PK',
			width    : 40,
			sortable : true,
			dataIndex: 'pk'
		},
		{
			text     : 'sleeptime',
			width    : 115,
			sortable : true,
			dataIndex: 'timestamp_sleep',
			renderer	: Ext.util.Format.dateRenderer('d.m.Y H:i')
		},
		{
			text     : 'waketime',
			width    : 115,
			sortable : true,
			dataIndex: 'timestamp_wake',
			renderer	: Ext.util.Format.dateRenderer('d.m.Y H:i')
		},
		{
			text     : 'Min',
			width    : 40,
			sortable : true,
			dataIndex: 'minutes'
		},
		{
			text     : 'Note',
			width    : 150,
			sortable : true,
			dataIndex: 'note',
			editor		: 'textfield'
		},
		{
			text     : 'TZA',
			width    : 40,
			sortable : true,
			dataIndex: 'timezone_adjustment'
		},
		{
			text     : 'DS',
			width    : 40,
			sortable : true,
			dataIndex: 'bool_daylight_saving'
		},
		{
			text     : 'ST',
			width    : 40,
			sortable : true,
			dataIndex: 'bool_standard_time'
		},
		{
			text     : 'inserted',
			width    : 100,
			sortable : true,
			dataIndex: 'timestamp_inserted',
			renderer	: Ext.util.Format.dateRenderer('d.m.Y')
		},
		{
			text     : 'Updated',
			width    : 100,
			sortable : true,
			dataIndex: 'timestamp_updated',
			renderer	: Ext.util.Format.dateRenderer('d.m.Y')
		}
	],

	listeners: {
		selectionchange: function(model, records) {
			console.log('selectionchange');

			var chart = Ext.ComponentQuery.query('trendview')[0];
			var series = chart.series.items[0];
			var selectedIndex = records[0].index;
			var object = series.items[ selectedIndex ];

			series.eachRecord( function(){ series.unHighlightItem(this); }, this);
			
			series.highlightItem( object );
			
			//console.log( object );
			
			//console.log( 'id = ' + this.itemId );
			//console.log( this.ownerCt.ownerCt );
			
			//var commissionRuleDetails = this.up('mainlayout').getComponent('panel-commissionruledetails');
			//var theForm = commissionRuleDetails.getForm();

			//if (records[0]) {
			//	if ( theForm.isDirty() ){
			//		commissionRuleDetails.askForAcceptance(records[0]);
			//	} else {
			//		theForm.loadRecord(records[0]);
			//		commissionRuleDetails.enableCopy(false);
			//	}
			//}
		}
		,
		edit: function(editor, e, eOpts) {
			console.log('update [table] set note = "' + e.value + '" where pk =' + e.record.data.pk);
//			console.log( e.record.data.pk );
		}
	},


	initComponent: function() {
		console.log('SleepRecordGrid()'); 
		this.callParent();
	}

});
