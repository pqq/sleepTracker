/*
	what:		Showing the stats for our dataThe presentation view, for a (visual) presentation of the data
	started:	April 2013
	who:		Frode Klevstul (frode at klevstul dot com)
*/


/*

http://docs.sencha.com/ext-js/4-2/extjs-build/examples/charts/Line.html
http://docs.sencha.com/ext-js/4-2/extjs-build/examples/charts/Line.js
http://docs.sencha.com/ext-js/4-2/extjs-build/examples/example-data.js

*/

generateData = function(n, floor){
        var data = [],
            p = (Math.random() *  11) + 1,
            i;
            
        floor = (!floor && floor !== 0)? 0 : floor;
        
        for (i = 0; i < (n || 12); i++) {
            data.push({
                timestamp_sleep: Ext.Date.monthNames[i % 12],
                minutes: Math.floor(Math.max((Math.random() * 10), floor)),
            });
        }
        return data;
    };

var store1 = Ext.create('Ext.data.JsonStore', {
        fields: ['timestamp_sleep', 'minutes'],
        data: generateData()
    });

//store1.loadData(generateData(8));

var store2 = Ext.create('Ext.data.JsonStore', {
//    extend: 'Ext.data.Store',
    model: 'SleepTracker.model.SleepRecord',
	pageSize: 5, // to be changed when paging is fully implemented
	// the following data section is to be removed and replaced with an ajax call in the model that fetches data from a given url
	data:{'results':[
        {
            'xaxis': 10,
            'yaxis': 1
        },
        {
            'xaxis': 20,
            'yaxis': 2
		}
    ]}
});



//console.log( store1 );


Ext.define('SleepTracker.view.TrendView', {

/*
	extend:		'Ext.panel.Panel',
	alias:		'widget.trendview',
	itemId:		'trendview',
	plain:		true,
	items: {
			html: 'trends'
		},
	drawchart: function(){
		console.log('drawchart()');
	}
*/



		extend:		'Ext.chart.Chart',
		alias:		'widget.trendview',
		itemId: 'trendview',
width: 800,
height: 400,
            style: 'background:#fff',
            animate: true,
            //store: store1,
			store: 'SleepRecords',
			//store: null,
            shadow: true,
            theme: 'Blue',
		gridName: null,
            axes: [{
                type: 'Numeric',
                minimum: 0,
                position: 'left',
                fields: ['minutes'],
                title: 'Minutes',
                minorTickSteps: 1,
                grid: {
                    odd: {
                        opacity: 1,
                        fill: '#ddd',
                        stroke: '#bbb',
                        'stroke-width': 0.1
                    }
                }
            }, {
                type: 'Category',
                position: 'bottom',
                fields: ['timestamp_sleep'],
                title: 'Day'
            }],
            series: [
			{
                type: 'line',
                highlight: {
                    size: 7,
                    radius: 7
                },
                axis: 'left',
                smooth: true,
                fill: true,
                xField: 'timestamp_sleep',
                yField: 'minutes',
                markerConfig: {
                    type: 'circle',
                    size: 4,
                    radius: 4,
                    'stroke-width': 0
                }
				,
				tips: {
				  trackMouse: true,
				  width: 300,
				  height: 80,
				  renderer: function(storeItem, item) {
					this.setTitle(storeItem.get('timestamp_sleep'));
					var hours = storeItem.get('minutes') / 60;
					hours = Math.round( hours * 10 ) / 10;
					this.update(hours + ' hours' + '<br/><br/>' + storeItem.get('note'));
//console.log( this.up('trendview') );

			//console.log( this.up('mainlayout') );


// http://stackoverflow.com/questions/15783648/how-to-set-selected-row-in-default-when-grid-load-using-checkboxmodel
					
					//console.log( storeItem.data.pk );
					//console.log( storeItem );
					//console.log( item );
					
					//console.log( Ext.ComponentQuery.query('sleeprecordgrid') );
					//var grid = Ext.ComponentQuery.query('sleeprecordgrid');
					
					//var grid = Ext.getCmp('mainlayout');
					
//console.log(grid);
					//var grid = this.up('mainlayout').getComponent('mainlayout_west').getComponent('sleeprecordgrid');
//					var sm = grid.getSelectionModel();
//console.log(sm);
//						Ext.each(records, function(record) {
//							if(record.data.pk === 2){
//							  var row = record.index;
//							  sm.select(row, true);
//							}
//						});

				  }
				}

					,
					listeners: {
						itemmousedown : function(obj) {
								//alert(obj.storeItem.data['pk'] + ' : ' + obj.storeItem.data['note']);
								//console.log( obj );
								//console.log( this );
								
								
								//var grid = Ext.ComponentQuery.query('sleeprecordgrid')[0]; // ComponentQuery returns an array, hence we need to use [0] to get the first element
								
				var gridName = Ext.ComponentQuery.query('trendview')[0].gridName;

		console.log( gridName );
				
								
								var grid = Ext.ComponentQuery.query( gridName )[0]; // ComponentQuery returns an array, hence we need to use [0] to get the first element
								
								//console.log( grid );
								//console.log( grid.getSize() );

								var sm = grid.getSelectionModel();
								//console.log(sm);
								sm.select( obj.storeItem.index , true);
								
								//Ext.each(records, function(record) {
								//	if(record.data.pk === 2){
								//	  var row = record.index;
								//	  sm.select(row, true);
								//	}
								//});


							}
					}
				}
            ]
		,
	listeners: {
		click: function(e, eOpts) {
			console.log('click chart');
			//console.log( this.up('mainlayout') );
			//console.log( e );
		}
	}

,
	setStore: function(store, gridname){
		console.log('trendView().bindStore()');
		//this.store = store;
console.log( 'TrendView.' + gridname );
		this.gridName = gridname;
//console.log( this.gridName );
		this.bindStore(store);

		
	}


,
	initComponent: function() {
		console.log('TrendView()');
		this.callParent();
	}

	
});