/*==============================================================*/
/* DBMS name:      ORACLE Version 11g                           */
/* Created on:     6/05/2013 12:38:26 PM                        */
/*==============================================================*/


drop trigger TIB_IMP_SLEEPBOT
/

drop trigger TRI_IMP_SLEEPBOT
/

drop trigger TIB_LOG_DEBUG
/

drop trigger TRI_LOG_DEBUG
/

drop trigger TIB_LOG_EXCEPTION
/

drop trigger TRI_LOG_EXCEPTION
/

drop trigger TIB_LOG_USER_ACTIVITY
/

drop trigger TRI_LOG_USER_ACTIVITY
/

drop trigger TIB_REC_SLEEP_RECORD
/

drop trigger TRI_REC_SLEEP_RECORD
/

drop trigger TRI_REC_USE_JOIN
/

drop trigger TIB_SES_SESSION
/

drop trigger TRI_SES_SESSION
/

drop trigger TIB_SRC_FILES
/

drop trigger TRI_SRC_FILES
/

drop trigger TIB_USE_IP_RESTRICTION
/

drop trigger TRI_USE_IP_RESTRICTION
/

drop trigger TIB_USE_USER
/

drop trigger TRI_USE_USER
/

alter table REC_USE_JOIN
   drop constraint FK_REC_USE__REFERENCE_REC_SLEE
/

alter table REC_USE_JOIN
   drop constraint FK_REC_USE__REFERENCE_USE_USER
/

alter table SES_SESSION
   drop constraint FK_SES_SESS_REFERENCE_USE_USER
/

alter table USE_IP_RESTRICTION
   drop constraint FK_USE_IP_R_REFERENCE_USE_USER
/

drop table IMP_SLEEPBOT cascade constraints
/

drop table IMP_SLEEPBOT_DUMP cascade constraints
/

drop table LOG_DEBUG cascade constraints
/

drop table LOG_EXCEPTION cascade constraints
/

drop table LOG_USER_ACTIVITY cascade constraints
/

drop table REC_SLEEP_RECORD cascade constraints
/

drop table REC_USE_JOIN cascade constraints
/

drop table SES_SESSION cascade constraints
/

drop table SRC_FILES cascade constraints
/

drop table SYS_SYSTEM cascade constraints
/

drop table USE_IP_RESTRICTION cascade constraints
/

drop table USE_USER cascade constraints
/

drop sequence SEQ_IMP_SLEEPBOT
/

drop sequence SEQ_LOG_DEBUG
/

drop sequence SEQ_LOG_EXCEPTION
/

drop sequence SEQ_LOG_USER_ACTIVITY
/

drop sequence SEQ_REC_SLEEP_RECORD
/

drop sequence SEQ_SES_SESSION
/

drop sequence SEQ_SRC_FILES
/

drop sequence SEQ_USE_IP_RESTRICTION
/

drop sequence SEQ_USE_USER
/

create sequence SEQ_IMP_SLEEPBOT
/

create sequence SEQ_LOG_DEBUG
/

create sequence SEQ_LOG_EXCEPTION
/

create sequence SEQ_LOG_USER_ACTIVITY
/

create sequence SEQ_REC_SLEEP_RECORD
/

create sequence SEQ_SES_SESSION
/

create sequence SEQ_SRC_FILES
/

create sequence SEQ_USE_IP_RESTRICTION
/

create sequence SEQ_USE_USER
/

/*==============================================================*/
/* Table: IMP_SLEEPBOT                                          */
/*==============================================================*/
create table IMP_SLEEPBOT 
(
   PK                   INT                  not null,
   TIMESTAMP_SLEEP      TIMESTAMP,
   TIMESTAMP_WAKE       TIMESTAMP,
   HOURS                NUMBER,
   NOTE                 VARCHAR2(100),
   FIX_MESSAGE          VARCHAR2(1000),
   IGNORE_MESSAGE       VARCHAR2(1000),
   BOOL_DAYLIGHT_SAVING INT                 
      constraint CKC_BOOL_DAYLIGHT_SAV_IMP_SLEE check (BOOL_DAYLIGHT_SAVING is null or (BOOL_DAYLIGHT_SAVING in (1))),
   BOOL_STANDARD_TIME   INT                 
      constraint CKC_BOOL_STANDARD_TIM_IMP_SLEE check (BOOL_STANDARD_TIME is null or (BOOL_STANDARD_TIME in (1))),
   BOOL_IMPORTED        INT                 
      constraint CKC_BOOL_IMPORTED_IMP_SLEE check (BOOL_IMPORTED is null or (BOOL_IMPORTED in (1))),
   BOOL_IGNORE          INT                 
      constraint CKC_BOOL_IGNORE_IMP_SLEE check (BOOL_IGNORE is null or (BOOL_IGNORE in (1))),
   TIMESTAMP_INSERT     TIMESTAMP,
   TIMESTAMP_UPDATE     TIMESTAMP,
   constraint PK_IMP_SLEEPBOT primary key (PK),
   constraint AK_KEY_2_IMP_SLEE unique (TIMESTAMP_SLEEP, TIMESTAMP_WAKE)
)
/

comment on table IMP_SLEEPBOT is
'import table for data from sleepbot app, data is stored in this table, that works as a intermedium table'
/

comment on column IMP_SLEEPBOT.TIMESTAMP_SLEEP is
'sleepbot data converted into a timestamp'
/

comment on column IMP_SLEEPBOT.TIMESTAMP_WAKE is
'sleepbot data converted into a timestamp'
/

comment on column IMP_SLEEPBOT.NOTE is
'note made in sleepbot'
/

comment on column IMP_SLEEPBOT.FIX_MESSAGE is
'message made related a fix'
/

comment on column IMP_SLEEPBOT.IGNORE_MESSAGE is
'explaning why entry is ignored'
/

comment on column IMP_SLEEPBOT.BOOL_DAYLIGHT_SAVING is
'true if sleep entry is during a switch to daylight saving'
/

comment on column IMP_SLEEPBOT.BOOL_STANDARD_TIME is
'true if sleep entry is during a switch to standard time'
/

comment on column IMP_SLEEPBOT.BOOL_IMPORTED is
'if flagged data has been imported into rec_sleeprecord'
/

comment on column IMP_SLEEPBOT.BOOL_IGNORE is
'if flagged row will not be imported to rec_sleeprecord'
/

/*==============================================================*/
/* Table: IMP_SLEEPBOT_DUMP                                     */
/*==============================================================*/
create table IMP_SLEEPBOT_DUMP 
(
   SLEEP_DATE           VARCHAR2(10),
   SLEEP_TIME           VARCHAR2(5),
   WAKE_TIME            VARCHAR2(5),
   HOURS                VARCHAR2(5),
   NOTE                 VARCHAR2(100)
)
/

comment on table IMP_SLEEPBOT_DUMP is
'table for dumping the cvs export file from sleepbot, table will be deleted in between further import'
/

/*==============================================================*/
/* Table: LOG_DEBUG                                             */
/*==============================================================*/
create table LOG_DEBUG 
(
   PK                   INT                  not null,
   MESSAGE              VARCHAR2(4000),
   TIMESTAMP_INSERT     TIMESTAMP,
   TIMESTAMP_UPDATE     TIMESTAMP,
   constraint PK_LOG_DEBUG primary key (PK)
)
/

comment on table LOG_DEBUG is
'for debugging purposes'
/

/*==============================================================*/
/* Table: LOG_EXCEPTION                                         */
/*==============================================================*/
create table LOG_EXCEPTION 
(
   PK                   INT,
   ORIGIN               VARCHAR2(50),
   ERROR_CODE           VARCHAR2(30),
   ERROR_MESSAGE        VARCHAR2(1000),
   ERROR_BACKTRACE      VARCHAR2(1000),
   BOOL_FIXED           INT                 
      constraint CKC_BOOL_FIXED_LOG_EXCE check (BOOL_FIXED is null or (BOOL_FIXED in (1))),
   TIMESTAMP_INSERT     TIMESTAMP,
   TIMESTAMP_UPDATE     TIMESTAMP
)
/

comment on table LOG_EXCEPTION is
'logging of exceptions'
/

/*==============================================================*/
/* Table: LOG_USER_ACTIVITY                                     */
/*==============================================================*/
create table LOG_USER_ACTIVITY 
(
   PK                   INT                  not null,
   USERNAME             VARCHAR2(100),
   CODE_MODULE          VARCHAR2(100),
   PARAMETER_LIST       VARCHAR2(500),
   TIMESTAMP_INSERT     TIMESTAMP,
   TIMESTAMP_UPDATE     TIMESTAMP,
   constraint PK_LOG_USER_ACTIVITY primary key (PK)
)
/

comment on table LOG_USER_ACTIVITY is
'log of user activity'
/

/*==============================================================*/
/* Table: REC_SLEEP_RECORD                                      */
/*==============================================================*/
create table REC_SLEEP_RECORD 
(
   PK                   INT                  not null,
   TIMESTAMP_SLEEP      TIMESTAMP            not null,
   TIMESTAMP_WAKE       TIMESTAMP            not null,
   MINUTES              NUMBER,
   NOTE                 VARCHAR2(1000),
   TIMEZONE_ADJUSTMENT  INT,
   BOOL_DAYLIGHT_SAVING INT                 
      constraint CKC_BOOL_DAYLIGHT_SAV_REC_SLEE check (BOOL_DAYLIGHT_SAVING is null or (BOOL_DAYLIGHT_SAVING in (1))),
   BOOL_STANDARD_TIME   INT                 
      constraint CKC_BOOL_STANDARD_TIM_REC_SLEE check (BOOL_STANDARD_TIME is null or (BOOL_STANDARD_TIME in (1))),
   TIMESTAMP_INSERT     TIMESTAMP,
   TIMESTAMP_UPDATE     TIMESTAMP,
   constraint PK_REC_SLEEP_RECORD primary key (PK),
   constraint AK_KEY_2_REC_SLEE unique (TIMESTAMP_SLEEP),
   constraint AK_KEY_3_REC_SLEE unique (TIMESTAMP_WAKE)
)
/

comment on table REC_SLEEP_RECORD is
'sleep records'
/

comment on column REC_SLEEP_RECORD.TIMESTAMP_SLEEP is
'time going to sleep'
/

comment on column REC_SLEEP_RECORD.TIMESTAMP_WAKE is
'time waking up'
/

comment on column REC_SLEEP_RECORD.MINUTES is
'number of minutes slept'
/

comment on column REC_SLEEP_RECORD.NOTE is
'note made regarding sleep record'
/

comment on column REC_SLEEP_RECORD.TIMEZONE_ADJUSTMENT is
'plus minus adjustment in hours'
/

comment on column REC_SLEEP_RECORD.BOOL_DAYLIGHT_SAVING is
'sleep period during transition to daylight savings (summer time)'
/

comment on column REC_SLEEP_RECORD.BOOL_STANDARD_TIME is
'sleep period during transition to standard time'
/

/*==============================================================*/
/* Table: REC_USE_JOIN                                          */
/*==============================================================*/
create table REC_USE_JOIN 
(
   REC_SLEEPRECORD_PK   INT                  not null,
   USE_USER_PK          INT                  not null,
   TIMESTAMP_INSERT     TIMESTAMP,
   TIMESTAMP_UPDATE     TIMESTAMP,
   constraint PK_REC_USE_JOIN primary key (REC_SLEEPRECORD_PK, USE_USER_PK)
)
/

comment on table REC_USE_JOIN is
'what record belongs to what user'
/

/*==============================================================*/
/* Table: SES_SESSION                                           */
/*==============================================================*/
create table SES_SESSION 
(
   PK                   INT                  not null,
   SESSION_KEY          VARCHAR2(40),
   USE_USER_FK          INT,
   IP_ADDRESS           VARCHAR2(39),
   LAST_CODE_MODULE     VARCHAR2(100),
   LAST_PARAMETER_LIST  VARCHAR2(500),
   RETURN_MESSAGE       VARCHAR2(500),
   TIMESTAMP_LAST_ACTION TIMESTAMP,
   TIMEOUT              INT,
   BOOL_KILLED          INT                 
      constraint CKC_BOOL_KILLED_SES_SESS check (BOOL_KILLED is null or (BOOL_KILLED in (1))),
   TIMESTAMP_INSERT     TIMESTAMP,
   TIMESTAMP_UPDATE     TIMESTAMP,
   constraint PK_SES_SESSION primary key (PK),
   constraint AK_KEY_2_SES_SESS unique (SESSION_KEY)
)
/

comment on table SES_SESSION is
'store session for users'
/

comment on column SES_SESSION.TIMEOUT is
'timeout in minutes'
/

comment on column SES_SESSION.BOOL_KILLED is
'if true the session is invalid'
/

/*==============================================================*/
/* Table: SRC_FILES                                             */
/*==============================================================*/
create table SRC_FILES 
(
   PK                   INT                  not null,
   FILENAME             VARCHAR2(100),
   CONTENT              CLOB,
   APPLICATION          VARCHAR2(100),
   TIMESTAMP_INSERT     TIMESTAMP,
   TIMESTAMP_UPDATE     TIMESTAMP,
   constraint PK_SRC_FILES primary key (PK)
)
/

comment on table SRC_FILES is
'source files, exported from sleep tracking applications and stored for backup purposes'
/

/*==============================================================*/
/* Table: SYS_SYSTEM                                            */
/*==============================================================*/
create table SYS_SYSTEM 
(
   TIMEOUT              INT
)
/

comment on table SYS_SYSTEM is
'system variables and their respective values
table should only contain ONE record'
/

/*==============================================================*/
/* Table: USE_IP_RESTRICTION                                    */
/*==============================================================*/
create table USE_IP_RESTRICTION 
(
   PK                   INT                  not null,
   USE_USER_FK          INT,
   IP_ADDRESS           VARCHAR2(39),
   TIMESTAMP_INSERT     TIMESTAMP,
   TIMESTAMP_UPDATE     TIMESTAMP,
   constraint PK_USE_IP_RESTRICTION primary key (PK)
)
/

comment on table USE_IP_RESTRICTION is
'defines from what IP addresses a user can log in'
/

/*==============================================================*/
/* Table: USE_USER                                              */
/*==============================================================*/
create table USE_USER 
(
   PK                   INT                  not null,
   USERNAME             VARCHAR2(100),
   PASSWORD             VARCHAR2(100),
   DESCRIPTION          VARCHAR2(500),
   LAST_SESSION_VK      INT,
   TIMESTAMP_INSERT     TIMESTAMP,
   TIMESTAMP_UPDATE     TIMESTAMP,
   constraint PK_USE_USER primary key (PK),
   constraint AK_KEY_2_USE_USER unique (USERNAME)
)
/

comment on table USE_USER is
'users of the solution'
/

comment on column USE_USER.LAST_SESSION_VK is
'a virtual reference to the last session used by this user'
/

alter table REC_USE_JOIN
   add constraint FK_REC_USE__REFERENCE_REC_SLEE foreign key (REC_SLEEPRECORD_PK)
      references REC_SLEEP_RECORD (PK)
/

alter table REC_USE_JOIN
   add constraint FK_REC_USE__REFERENCE_USE_USER foreign key (USE_USER_PK)
      references USE_USER (PK)
/

alter table SES_SESSION
   add constraint FK_SES_SESS_REFERENCE_USE_USER foreign key (USE_USER_FK)
      references USE_USER (PK)
/

alter table USE_IP_RESTRICTION
   add constraint FK_USE_IP_R_REFERENCE_USE_USER foreign key (USE_USER_FK)
      references USE_USER (PK)
/


create trigger TIB_IMP_SLEEPBOT before insert
on IMP_SLEEPBOT for each row
declare
    integrity_error  exception;
    errno            integer;
    errmsg           char(200);
    dummy            integer;
    found            boolean;

begin
    --  Column "PK" uses sequence SEQ_IMP_SLEEPBOT
    select SEQ_IMP_SLEEPBOT.NEXTVAL INTO :new.PK from dual;

--  Errors handling
exception
    when integrity_error then
       raise_application_error(errno, errmsg);
end;
/


create trigger TRI_IMP_SLEEPBOT
before insert or update
on IMP_SLEEPBOT 
for each row
begin
	if inserting then
		:new.timestamp_insert := systimestamp;
	elsif updating then
		:new.timestamp_update := systimestamp;
	end if;
end;
/


create trigger TIB_LOG_DEBUG before insert
on LOG_DEBUG for each row
declare
    integrity_error  exception;
    errno            integer;
    errmsg           char(200);
    dummy            integer;
    found            boolean;

begin
    --  Column "PK" uses sequence SEQ_LOG_DEBUG
    select SEQ_LOG_DEBUG.NEXTVAL INTO :new.PK from dual;

--  Errors handling
exception
    when integrity_error then
       raise_application_error(errno, errmsg);
end;
/


create trigger TRI_LOG_DEBUG
before insert or update
on LOG_DEBUG 
for each row
begin
	if inserting then
		:new.timestamp_insert := systimestamp;
	elsif updating then
		:new.timestamp_update := systimestamp;
	end if;
end;
/


create trigger TIB_LOG_EXCEPTION before insert
on LOG_EXCEPTION for each row
declare
    integrity_error  exception;
    errno            integer;
    errmsg           char(200);
    dummy            integer;
    found            boolean;

begin
    --  Column "PK" uses sequence SEQ_LOG_EXCEPTION
    select SEQ_LOG_EXCEPTION.NEXTVAL INTO :new.PK from dual;

--  Errors handling
exception
    when integrity_error then
       raise_application_error(errno, errmsg);
end;
/


create trigger TRI_LOG_EXCEPTION
before insert or update
on LOG_EXCEPTION 
for each row
begin
	if inserting then
		:new.timestamp_insert := systimestamp;
	elsif updating then
		:new.timestamp_update := systimestamp;
	end if;
end;
/


create trigger TIB_LOG_USER_ACTIVITY before insert
on LOG_USER_ACTIVITY for each row
declare
    integrity_error  exception;
    errno            integer;
    errmsg           char(200);
    dummy            integer;
    found            boolean;

begin
    --  Column "PK" uses sequence SEQ_LOG_USER_ACTIVITY
    select SEQ_LOG_USER_ACTIVITY.NEXTVAL INTO :new.PK from dual;

--  Errors handling
exception
    when integrity_error then
       raise_application_error(errno, errmsg);
end;
/


create trigger TRI_LOG_USER_ACTIVITY
before insert or update
on LOG_USER_ACTIVITY 
for each row
begin
	if inserting then
		:new.timestamp_insert := systimestamp;
	elsif updating then
		:new.timestamp_update := systimestamp;
	end if;
end;
/


create trigger TIB_REC_SLEEP_RECORD before insert
on REC_SLEEP_RECORD for each row
declare
    integrity_error  exception;
    errno            integer;
    errmsg           char(200);
    dummy            integer;
    found            boolean;

begin
    --  Column "PK" uses sequence SEQ_REC_SLEEP_RECORD
    select SEQ_REC_SLEEP_RECORD.NEXTVAL INTO :new.PK from dual;

--  Errors handling
exception
    when integrity_error then
       raise_application_error(errno, errmsg);
end;
/


create trigger TRI_REC_SLEEP_RECORD
before insert or update
on REC_SLEEP_RECORD 
for each row
begin
	if inserting then
		:new.timestamp_insert := systimestamp;
	elsif updating then
		:new.timestamp_update := systimestamp;
	end if;
end;
/


create trigger TRI_REC_USE_JOIN
before insert or update
on REC_USE_JOIN 
for each row
begin
	if inserting then
		:new.timestamp_insert := systimestamp;
	elsif updating then
		:new.timestamp_update := systimestamp;
	end if;
end;
/


create trigger TIB_SES_SESSION before insert
on SES_SESSION for each row
declare
    integrity_error  exception;
    errno            integer;
    errmsg           char(200);
    dummy            integer;
    found            boolean;

begin
    --  Column "PK" uses sequence SEQ_SES_SESSION
    select SEQ_SES_SESSION.NEXTVAL INTO :new.PK from dual;

--  Errors handling
exception
    when integrity_error then
       raise_application_error(errno, errmsg);
end;
/


create trigger TRI_SES_SESSION
before insert or update
on SES_SESSION 
for each row
begin
	if inserting then
		:new.timestamp_insert := systimestamp;
	elsif updating then
		:new.timestamp_update := systimestamp;
	end if;
end;
/


create trigger TIB_SRC_FILES before insert
on SRC_FILES for each row
declare
    integrity_error  exception;
    errno            integer;
    errmsg           char(200);
    dummy            integer;
    found            boolean;

begin
    --  Column "PK" uses sequence SEQ_SRC_FILES
    select SEQ_SRC_FILES.NEXTVAL INTO :new.PK from dual;

--  Errors handling
exception
    when integrity_error then
       raise_application_error(errno, errmsg);
end;
/


create trigger TRI_SRC_FILES
before insert or update
on SRC_FILES 
for each row
begin
	if inserting then
		:new.timestamp_insert := systimestamp;
	elsif updating then
		:new.timestamp_update := systimestamp;
	end if;
end;
/


create trigger TIB_USE_IP_RESTRICTION before insert
on USE_IP_RESTRICTION for each row
declare
    integrity_error  exception;
    errno            integer;
    errmsg           char(200);
    dummy            integer;
    found            boolean;

begin
    --  Column "PK" uses sequence SEQ_USE_IP_RESTRICTION
    select SEQ_USE_IP_RESTRICTION.NEXTVAL INTO :new.PK from dual;

--  Errors handling
exception
    when integrity_error then
       raise_application_error(errno, errmsg);
end;
/


create trigger TRI_USE_IP_RESTRICTION
before insert or update
on USE_IP_RESTRICTION 
for each row
begin
	if inserting then
		:new.timestamp_insert := systimestamp;
	elsif updating then
		:new.timestamp_update := systimestamp;
	end if;
end;
/


create trigger TIB_USE_USER before insert
on USE_USER for each row
declare
    integrity_error  exception;
    errno            integer;
    errmsg           char(200);
    dummy            integer;
    found            boolean;

begin
    --  Column "PK" uses sequence SEQ_USE_USER
    select SEQ_USE_USER.NEXTVAL INTO :new.PK from dual;

--  Errors handling
exception
    when integrity_error then
       raise_application_error(errno, errmsg);
end;
/


create trigger TRI_USE_USER
before insert or update
on USE_USER 
for each row
begin
	if inserting then
		:new.timestamp_insert := systimestamp;
	elsif updating then
		:new.timestamp_update := systimestamp;
	end if;
end;
/

