PROMPT *** log_lib ***
set define off
set serveroutput on





-- ******************************************************
-- *													*
-- *				package head						*
-- *													*
-- ******************************************************
create or replace package log_lib is
	-- +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	-- WARNING: Never edit this code directly using TOAD or another Oracle tool.
	--          The updates should always be done editing the original script, and
	--          then be compiled into the database using TOAD or SQL*Plus !!!
	-- +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

	-- +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	-- Package overview:
	-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	-- name: log_lib
	--
	-- Package containing logging related code.
	--
	-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	-- How to make comments in the code:
	-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	-- ex 1:
	-- ----------------------------------------------------------------------
	-- Comment a bulk of code
	-- For example if you want to comment the next portion of the code, like
	-- an while loop, if statment or something similar.
	-- ----------------------------------------------------------------------

	-- ex 2:
	-- ---
	-- important comment for the next line(s) of code
	-- ---

	-- ex 3:
	-- comment the next line(s) of code

	-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	--
	-- *** Revision | Newest version at the top
	--
	-- 130209 : Frode Klevstul : started
	--
	-- +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	procedure i;
	procedure debug
	(
		p_message					in log_debug.message%type
	);
	procedure exceptionHandle
	(
		  p_error_code				in log_exception.error_code%type
		, p_error_message			in log_exception.error_message%type
		, p_origin					in log_exception.origin%type
	);
	procedure userActivity
	(
		  p_session_key				in log_user_activity.session_key%type
	    , p_username				in log_user_activity.username%type
		, p_code_module				in log_user_activity.code_module%type
		, p_parameter_list			in log_user_activity.parameter_list%type
		, p_details					in log_user_activity.details%type
	);

end;
/
show errors;





-- ******************************************************
-- *													*
-- *				package body						*
-- *													*
-- ******************************************************
create or replace package body log_lib is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
	g_package	constant varchar2(16)	:= 'log_lib';
	g_debug		constant boolean		:= true;

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        i(nfo)
-- what:        info procedure
-- author:      Frode Klevstul
-- start date:  2013.02.09
---------------------------------------------------------
procedure i
is
	c_procedure					constant varchar2(32)					:= 'i';
begin
	dbms_output.put_line(to_char(sysdate,'YYMMDD HH24:MI:SS') ||': '||g_package||'.'||c_procedure);
end i;


---------------------------------------------------------
-- name:        debug
-- what:        insert debug information to the database
-- author:      Frode Klevstul
-- start date:  2013.02.10
---------------------------------------------------------
procedure debug
(
	p_message					in log_debug.message%type
)
is
	c_procedure					constant varchar2(32)					:= 'debug';
begin

	if (g_debug) then
		insert into log_debug
		(message)
		values(p_message);
		commit;
	end if;

exception
	when others then
		exceptionHandle(sqlcode, sqlerrm, g_package||'.'||c_procedure);
		null;
end debug;


---------------------------------------------------------
-- name:        exceptionHandle
-- what:        handles exceptions
-- author:      Frode Klevstul
-- start date:  2013.02.10
---------------------------------------------------------
procedure exceptionHandle
(
	  p_error_code				in log_exception.error_code%type
	, p_error_message			in log_exception.error_message%type
	, p_origin					in log_exception.origin%type
)
is
	c_procedure					constant varchar2(32)					:= 'exceptionHandle';

	v_errMsg1					varchar2(1024)							:= p_error_message;
	v_errMsg2					varchar2(1024)							:= dbms_utility.format_error_backtrace;
	v_errMsg3					varchar2(1024)							:= dbms_utility.format_error_stack;

begin

	-- ignore user-defined error messages with error code -20001
	if (p_error_code != -20001) then
		insert into log_exception
		(origin, error_code, error_message, error_backtrace)
		values (p_origin, p_error_code, p_error_message, v_errMsg2);
		commit;
	end if;

	dbms_output.put_line('
'||		p_origin||'
'||		p_error_message||'
'||		v_errMsg1||'
'||		v_errMsg2||'
'||		v_errMsg3||'
	');

end exceptionHandle;


---------------------------------------------------------
-- name:        userActivity
-- what:        log user activity
-- author:      Frode Klevstul
-- start date:  2013.05.06
---------------------------------------------------------
procedure userActivity
(
	  p_session_key				in log_user_activity.session_key%type
	, p_username				in log_user_activity.username%type
	, p_code_module				in log_user_activity.code_module%type
	, p_parameter_list			in log_user_activity.parameter_list%type
	, p_details					in log_user_activity.details%type
)
is
	c_procedure					constant varchar2(32)					:= 'userActivity';

	v_details					log_user_activity.details%type;

begin

	-- replace tabs with space
	v_details := replace(p_details, chr(9), ' ');

	-- remove two and more spaces
	v_details := regexp_replace(v_details, '[[:space:]]{2,}','');
	
	-- insert into the database
	insert into log_user_activity
	(session_key, username, code_module, parameter_list, details)
	values (p_session_key, p_username, p_code_module, p_parameter_list, v_details);
	commit;

exception
	when others then
		exceptionHandle(sqlcode, sqlerrm, g_package||'.'||c_procedure);
		null;
end userActivity;





-- ******************************************** --
end;
/
show errors;