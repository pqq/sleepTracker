PROMPT *** sys_lib ***
set define off
set serveroutput on





-- ******************************************************
-- *													*
-- *				package head						*
-- *													*
-- ******************************************************
create or replace package sys_lib is
	-- +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	-- WARNING: Never edit this code directly using TOAD or another Oracle tool.
	--          The updates should always be done editing the original script, and
	--          then be compiled into the database using TOAD or SQL*Plus !!!
	-- +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

	-- +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	-- Package overview:
	-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	-- name: sys_lib
	--
	-- Package containing module system related code.
	--
	-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	-- How to make comments in the code:
	-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	-- ex 1:
	-- ----------------------------------------------------------------------
	-- Comment a bulk of code
	-- For example if you want to comment the next portion of the code, like
	-- an while loop, if statment or something similar.
	-- ----------------------------------------------------------------------

	-- ex 2:
	-- ---
	-- important comment for the next line(s) of code
	-- ---

	-- ex 3:
	-- comment the next line(s) of code

	-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	--
	-- *** Revision | Newest version at the top
	--
	-- 130506 : Frode Klevstul : started
	--
	-- +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	procedure i;
	function getVariableValue
	(
		  p_var_name				in sys_variable.var_name%type
	) return sys_variable.var_value%type;

end;
/
show errors;





-- ******************************************************
-- *													*
-- *				package body						*
-- *													*
-- ******************************************************
create or replace package body sys_lib is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
	g_package	constant varchar2(16)	:= 'sys_lib';
	g_debug		constant boolean		:= false;

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        i(nfo)
-- what:        info procedure
-- author:      Frode Klevstul
-- start date:  2013.05.06
---------------------------------------------------------
procedure i
is
	c_procedure					constant varchar2(32)					:= 'i';
begin
	dbms_output.put_line(to_char(sysdate,'YYMMDD HH24:MI:SS') ||': '||g_package||'.'||c_procedure);
end i;


---------------------------------------------------------
-- name:        getVariableValue
-- what:        returns a system variable's value from sys_variable
-- author:      Frode Klevstul
-- start date:  2013.05.06
---------------------------------------------------------
function getVariableValue
(
	  p_var_name				in sys_variable.var_name%type
) return sys_variable.var_value%type
is
	c_procedure					constant varchar2(32)					:= 'getVariableValue';

	v_var_value					sys_variable.var_value%type;

begin

	for r_cursor in (
		select	var_value
		from	sys_variable
		where	var_name = p_var_name
	) loop
		v_var_value := r_cursor.var_value;
	end loop;

	if (v_var_value is null) then
		raise_application_error(-20000, 'no entry in sys_variable with var_name = <'||p_var_name||'>');
	end if;

	return v_var_value;

exception
	when others then
		log_lib.exceptionHandle(sqlcode, sqlerrm, g_package||'.'||c_procedure);
		return null;
end getVariableValue;





-- ******************************************** --
end;
/
show errors;