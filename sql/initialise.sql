delete from use_ip_restriction;
delete from ses_session;
delete from rec_use_join;
delete from use_user;
insert into use_user (username, password) values ('rpc', 'pwd');
insert into use_ip_restriction(use_user_fk, ip_address) values( (select pk from use_user where username='rpc'), '84.215.2.139');

delete from sys_variable;
insert into sys_variable (var_name, var_value) values ('timeout', '10');
insert into sys_variable (var_name, var_value) values ('rpc_date_format', 'YYYY-MM-DD HH24:MI:SS');
insert into sys_variable (var_name, var_value) values ('rpc_date_format_short', 'YYYY-MM-DD');

delete from hel_help;
insert into hel_help(pk, help_text) values(1, 'SLEEPTRACKER REMOTE PROCEDURE CALL (RPC) API HELP');
insert into hel_help(pk, help_text) values(2, 'All parameters: p_module, p_action, p_username, p_password, p_ip_address, p_session_key, p_id');

delete  from imp_sleepbot_dump;

set termout off
@../sql/imp_sleepbot_dump_mini.sql
set termout on
/

begin
	imp_lib.run;
end;
/

begin
	for r_cursor in(
		select	pk
		from	rec_sleep_record
	) loop
		insert into rec_use_join
		(rec_sleep_record_fk_pk, use_user_fk_pk)
		values(
			  r_cursor.pk
			, (select pk from use_user where username = 'rpc')
		);
	end loop;
end;
/

commit;
