/*
	what:		Global objects to store key data
	started:	March 2013
	who:		Frode Klevstul (frode at klevstul dot com)
*/
Ext.namespace('Global');

Global.session = new Object;
Global.session.username = '';
Global.session.sessionKey = '';

Global.parameters						= new Object;
Global.parameters.applicationName		= 'SleepTracker';
Global.parameters.applicationVersion	= '0.1.1305';
Global.parameters.credits				= 'Frode Klevstul (<a href="http://www.watashi.no" target="_blank">www.watashi.no</a>)';
Global.parameters.jsonBasePath			= '/cgi/';
Global.parameters.rpcMethod				= 'jsonRpc.py';
Global.parameters.rpcMethodFull			= Global.parameters.jsonBasePath + Global.parameters.rpcMethod;
Global.parameters.moduleParam			= 'p_module';
Global.parameters.actionParam			= 'p_action';
Global.parameters.sessionKey			= 'p_session_key';
