/*
	what:		Model for a sleep record
	started:	March 2013
	who:		Frode Klevstul (frode at klevstul dot com)

*/

Ext.define('SleepTracker.model.SleepRecord', {
      extend: 'Ext.data.Model'
	, fields: [
          {name: 'PK',  					type: 'int'}
		, {name: 'TIMESTAMP_SLEEP', 		type: 'date', 		dateFormat: 'Y-m-d H:i:s'}
		, {name: 'TIMESTAMP_WAKE', 			type: 'date', 		dateFormat: 'Y-m-d H:i:s'}
		, {name: 'MINUTES', 				type: 'int'}
		, {name: 'NOTE', 					type: 'string'}
		, {name: 'TIMEZONE_ADJUSTMENT',		type: 'int'}
		, {name: 'BOOL_DAYLIGHT_SAVING', 	type: 'int'}
		, {name: 'BOOL_STANDARD_TIME', 		type: 'int'}
		, {name: 'TIMESTAMP_INSERT', 		type: 'date', 		dateFormat: 'Y-m-d H:i:s'}
		, {name: 'TIMESTAMP_UPDATE', 		type: 'date', 		dateFormat: 'Y-m-d H:i:s'}
    ]
	, proxy: {
           type:	'ajax'
         , url:		''
         , reader: {
               type: 'json'
			 , root: 'result_set'
         }
     }
});
