/*
	what:		Model for a sleep record day (a day of sleep)
	started:	April 2013
	who:		Frode Klevstul (frode at klevstul dot com)

*/

Ext.define('SleepTracker.model.SleepRecordDay', {
      extend: 'Ext.data.Model'
	, fields: [
		  {name: 'PK',  				type: 'int'}
		, {name: 'TIMESTAMP_SLEEP',		type: 'date', 		dateFormat: 'Y-m-d'}
		, {name: 'MINUTES', 			type: 'int'}
    ]
	, proxy: {
		  type: 'ajax'
		, url: ''
		, reader: {
			  type: 'json'
			, root: 'result_set'
		  }
	  }
});
