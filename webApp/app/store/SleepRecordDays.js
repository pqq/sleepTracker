/*
	what:		Store for the sleep record day grid
	started:	April 2013
	who:		Frode Klevstul (frode at klevstul dot com)
*/

Ext.define('SleepTracker.store.SleepRecordDays', {
	  extend:	'Ext.data.Store'
	, model:	'SleepTracker.model.SleepRecordDay'
	, autoLoad:	false
});
