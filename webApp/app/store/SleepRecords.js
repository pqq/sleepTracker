/*
	what:		Store for the sleep record grid
	started:	March 2013
	who:		Frode Klevstul (frode at klevstul dot com)
*/

Ext.define('SleepTracker.store.SleepRecords', {
      extend:	'Ext.data.Store'
	, model:	'SleepTracker.model.SleepRecord'
	, autoLoad:	false
});
