/*
	what:		Utility functions
	started:	May 2013
	who:		Frode Klevstul (frode at klevstul dot com)
*/
Ext.namespace('SleepTracker.util');


//--------------------------------------------------------
// display message on screen
//--------------------------------------------------------
SleepTracker.util.showMessage = function(message){
	Ext.Msg.show({
		  title:	'Info'
	    , minWidth:	400
		, msg:		'\n' + message
		, buttons:	Ext.Msg.OK
		, icon:		Ext.Msg.INFO
	});
}


//--------------------------------------------------------
// display error message on screen
//--------------------------------------------------------
SleepTracker.util.showError = function(errorMsg){
	Ext.Msg.show({
		  title:	'Error'
	    , minWidth:	400
		, msg:		'\n' + errorMsg
		, buttons:	Ext.Msg.OK
		, icon:		Ext.Msg.ERROR
	});
}


//--------------------------------------------------------
// handles responses (AJAX, Form, Store etc)
// --------------------------------------------------------
SleepTracker.util.onResponse = function(response, handler, errorHandler){
	var errorMsg = null;
	
	if (response.status !== 200) {
		errorMsg = 'HTTP response: ' + response.status + ' ' + response.statusText;
	} else {
		var txt = response.responseText;
		if (txt === null || txt.length === 0) {
			errorMsg = 'No response';
		} else {
			var obj = Ext.decode(txt);
			if (! obj.success){
				SleepTracker.util.showError(obj.message);
			}
		}
	}
	
	if (errorMsg === null) {

		// no result_set object
		if (obj.result_set === undefined){
			SleepTracker.util.showError('RPC ERROR: JSON not containing "result_set".');
		}

		// no bool_error value in the result_set
		if (obj.result_set[0].BOOL_ERROR === undefined){
			SleepTracker.util.showError('RPC ERROR: JSON not containing "bool_error".');
		}
		
		// a general error
		if (obj.result_set[0].BOOL_ERROR === 1){
			SleepTracker.util.showError(obj.result_set[0].STATUS_MESSAGE);
		
		// session error
		} else if (obj.result_set[0].BOOL_ERROR === 2){
			SleepTracker.util.showMessage('Invalid session: ' + obj.result_set[0].STATUS_MESSAGE);

		// no error
		} else {
			if (handler)
				handler(obj);
		}

	} else {
		if (errorHandler)
			errorHandler(obj.statusCode, obj.errorMsg);
		else
			SleepTracker.util.showError(errorMsg);
	}
}


//--------------------------------------------------------
// Function to handle AJAX responses
// --------------------------------------------------------
SleepTracker.util.onAjaxResp = function(response, options) {
	SleepTracker.util.onResponse(response, this.handler, this.errorHandler);

	if (this.loadMask)
		this.loadMask.destroy();
};


//--------------------------------------------------------
// Function to handle form responses
//--------------------------------------------------------
SleepTracker.util.onFormResp = function(form, action) {
  var response = action.response;

  if ( (response === null || response === undefined) && action.failureType === Ext.form.Action.CLIENT_INVALID){
		SleepTracker.util.showError('Invalid fields in form');
	} else {
		SleepTracker.util.onResponse(response, this.handler, this.errorHandler);
	}

	if (this.loadMask)
		this.loadMask.destroy();
};


//--------------------------------------------------------
// Function to handle store responses
//--------------------------------------------------------
SleepTracker.util.onStoreResponse = function(records, options, success) {
	SleepTracker.util.onResponse(options.response, this.handler, this.errorHandler);
};

