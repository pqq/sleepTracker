/*
	what:		The login panel for the login window
	started:	May 2013
	who:		Frode Klevstul (frode at klevstul dot com)
*/

Ext.define('SleepTracker.view.LoginPanel', {
	  extend:		'Ext.FormPanel'
	, alias:		'widget.loginpanel'
	, itemId:		'loginpanel'
	, url:			Global.parameters.rpcMethodFull
	, frame:		false
	, border: 		false
	, bodyStyle:	'padding:5px 5px 5px 5px'
	, defaultType:	'textfield'
	, items: [
		{
			  extend:		'Ext.Data.Field'
			, name:			Global.parameters.moduleParam
			, inputType:	'hidden'
			, value:		'login'
		}
		,
		{
			  extend:		'Ext.Data.Field'
			,  name:		Global.parameters.actionParam
			, inputType:	'hidden'
			, value:		'verify'
        }
		,
		{
			  extend:		'Ext.Data.Field'
			, value: 		'rpc'
			, fieldLabel:	'Username'
			, labelWidth:	75
			, name:			'p_username'
			, width:		190
			, allowBlank:	false
			, blankText:	'.'
        }
		,
		{
			  extend:		'Ext.Data.Field'
			, value:		'pwd'
			, fieldLabel:	'Password'
			, labelWidth:	75
			, name:			'p_password'
			, width:		190
			, allowBlank:	false
			, inputType:	'password'
			, listeners: {
				'specialkey': function(f, e){
					if (e.getKey() == e.ENTER)
						this.up('loginwindow').getComponent('loginwindow_toolbar').getComponent('loginwindow_toolbar_button').fireEvent('click');						
				}
			  }
        }
	  ]
	, initComponent: function() {
		console.log('LoginPanel()');
		this.callParent();
	}
});
