/*
	what:		The login window
	started:	May 2013
	who:		Frode Klevstul (frode at klevstul dot com)
*/

Ext.define('SleepTracker.view.LoginWindow', {
	  extend:		'Ext.Window'
	, alias:		'widget.loginwindow'
	, itemId:		'loginwindow'
	, layout:		'fit'
	, stateful:		false
	, modal:		true
	, closable:		false
	, title: 		'Login to SleepTracker'
	, width: 		212
	, height:		150
	, items: 		{xtype: 'loginpanel'}
	, dockedItems: [
		{
			  xtype: 	'toolbar'
			, itemId:	'loginwindow_toolbar'
			, dock:		'bottom'
			, items: [
				  '->'
				, {
					  extend:		'Ext.Button'
					, itemId:		'loginwindow_toolbar_button'
					, tooltip:		'Login'
					, text: 		'Login'
					, listeners: {
						'click': function(button, event){
							var win = this.up('loginwindow');
							var myMask = new Ext.LoadMask(win, {msg:"Please wait..."});
							myMask.show();

							win.getComponent('loginpanel').getForm().submit({
								  success: SleepTracker.util.onFormResp
								, failure: SleepTracker.util.onFormResp
								, scope: {
									  loadMask: myMask
									, handler: function(obj){
										var loginText = new Array(
											  "is running the show"
											, "is being served by " + Global.parameters.applicationName
											, "is in da house"
											, "is rockin"
											, "is fooling around"
											, "is the Master"
											, "is in control"
											, "has got the power"
											, "is ohh la la"
											, "is legend"
											, "is logged in"
											, "for King"
										);
										var len = loginText.length;
										var rnd_no = Math.floor(len*Math.random());

										Global.session.sessionKey = obj.result_set[0].SESSION_KEY;
										Global.session.username = obj.result_set[0].USERNAME;

										if (
											(Global.session.sessionKey === null || Global.session.sessionKey === undefined)
											||
											(Global.session.username === null || Global.session.username === undefined)
										){
											SleepTracker.util.showError('RPC ERROR : "SESSION_KEY" and/or "USERNAME" missing from JSON.');

										// successful login...
										} else {
											win.destroy();

											Ext.getStore('SleepRecords').proxy.url = Global.parameters.rpcMethodFull + '?' + Global.parameters.moduleParam + '=sleep_record&' + Global.parameters.actionParam + '=list&p_session_key=' + Global.session.sessionKey;
											Ext.getStore('SleepRecordDays').proxy.url = Global.parameters.rpcMethodFull + '?' + Global.parameters.moduleParam + '=sleep_record_day&' + Global.parameters.actionParam + '=list&p_session_key=' + Global.session.sessionKey;
											Ext.getStore('SleepRecordDays').load({
												  callback: SleepTracker.util.onStoreResponse
												, scope: {
													handler: function(obj){
														Ext.getStore('SleepRecords').load({callback: SleepTracker.util.onStoreResponse});
													}
												  }
											});
										}

										document.getElementById('SleepTracker.MainLayout.south.loggedInMessage').innerHTML = Global.session.username + ' ' + loginText[rnd_no];
									}
								}
							});
						}
					}
				}
			]
		}
	]
	, initComponent: function() {
		console.log('LoginWindow()');
		this.callParent();
	}
});
