/*
	what:		Present all sleep records in a grid
	started:	April 2013
	who:		Frode Klevstul (frode at klevstul dot com)
*/

Ext.define('SleepTracker.view.SleepRecordDayGrid', {
      extend:	'Ext.grid.Panel'
	, itemId:	'sleeprecorddaygrid'
	, alias:	'widget.sleeprecorddaygrid'
	, viewConfig: {
		stripeRows: true
	  }
	, store:	'SleepRecordDays'
	, border:	false
	, layout:	'fit'
	, plugins: [
		Ext.create('Ext.grid.plugin.CellEditing', {clicksToEdit: 2})
      ]
	, columns: [
		{
			  text:			'pk'
			, width:		40
			, sortable:		true
			, dataIndex:	'PK'
		}
		,
		{
			  text: 'date'
			, width: 115
			, sortable : true
			, dataIndex: 'TIMESTAMP_SLEEP'
			, renderer: Ext.util.Format.dateRenderer('Y-m-d')
		}
		,
		{
			  text: 'min'
			, width: 40
			, sortable: true
			, dataIndex: 'MINUTES'
		}
	  ]
	, listeners: {
		  selectionchange: function(model, records) {
			if (records.length !== 0) {
				var chart = Ext.ComponentQuery.query('trendview')[0];
				var series = chart.series.items[0];
				var selectedIndex = records[0].index;
				var object = series.items[ selectedIndex ];

				series.eachRecord( function(){ series.unHighlightItem(this); }, this);
				series.highlightItem( object );
			}
		  }
		, itemdblclick: function(this_, record, item, index, e, eOpts) {
			var view = Ext.ComponentQuery.query('gridmainview')[0];
			var grid = Ext.ComponentQuery.query('sleeprecordgrid')[0];
			var store = grid.getStore();
			var sm = grid.getSelectionModel();			
			var rows = store.getRange();

			view.setActiveTab(1);
			Ext.each(rows, function(row) {
				if(row.data.PK === record.data.PK){
				  var idx = row.index;
				  sm.select(idx, true);
				}
			});
		  }
	  }
	, initComponent: function() {
		console.log('SleepRecordDayGrid()'); 
		this.callParent();
	}
});
