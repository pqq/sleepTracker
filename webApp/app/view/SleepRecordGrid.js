/*
	what:		Present all sleep records in a grid
	started:	March 2013
	who:		Frode Klevstul (frode at klevstul dot com)
*/

Ext.define('SleepTracker.view.SleepRecordGrid', {
      extend:	'Ext.grid.Panel'
	, itemId:	'sleeprecordgrid'
	, alias:	'widget.sleeprecordgrid'
	, viewConfig: {
		stripeRows: true
	  }
	, store: 	'SleepRecords'
	, border: 	false
	, layout: 	'fit'
	, plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {clicksToEdit: 2})
      ]
	, columns: [
		{
			  text: 		'pk'
			, width:		40
			, sortable:		true
			, dataIndex:	'PK'
		}
		,
		{
			  text:			'sleeptime'
			, width:		115
			, sortable:		true
			, dataIndex:	'TIMESTAMP_SLEEP'
			, renderer:		Ext.util.Format.dateRenderer('d.m.Y H:i')
		}
		,
		{
			  text:			'waketime'
			, width:		115
			, sortable:		true
			, dataIndex:	'TIMESTAMP_WAKE'
			, renderer:		Ext.util.Format.dateRenderer('d.m.Y H:i')
		}
		,
		{
			  text:			'min'
			, width:		40
			, sortable:		true
			, dataIndex:	'MINUTES'
		}
		,
		{
			  text:			'note'
			, width:		150
			, sortable:		true
			, dataIndex:	'NOTE'
			, editor:		'textfield'
		}
		,
		{
			  text:			'tza'
			, width:		40
			, sortable:		true
			, dataIndex:	'TIMEZONE_ADJUSTMENT'
		}
		,
		{
			  text:			'ds'
			, width:		40
			, sortable:		true
			, dataIndex:	'BOOL_DAYLIGHT_SAVING'
		}
		,
		{
			  text:			'st'
			, width:		40
			, sortable:		true
			, dataIndex:	'BOOL_STANDARD_TIME'
		}
		,
		{
			  text:			'insert'
			, width:		100
			, sortable:		true
			, dataIndex:	'TIMESTAMP_INSERT'
			, renderer:		Ext.util.Format.dateRenderer('d.m.Y')
		}
		,
		{
			  text:			'update'
			, width:		100
			, sortable:		true
			, dataIndex:	'TIMESTAMP_UPDATE'
			, renderer:		Ext.util.Format.dateRenderer('d.m.Y')
		}
	  ]
	, listeners: {
		selectionchange: function(model, records) {
			// check for avoiding error when reloading store while a row is selected
			if (records.length !== 0) {
				var chart = Ext.ComponentQuery.query('trendview')[0];
				var series = chart.series.items[0];
				var selectedIndex = records[0].index;
				var object = series.items[ selectedIndex ];

				series.eachRecord( function(){ series.unHighlightItem(this); }, this);
				series.highlightItem( object );
			}
		}
		, edit: function(editor, e, eOpts) {
			Ext.Ajax.request({
				  url: Global.parameters.rpcMethodFull
				, params: {
						p_session_key:	Global.session.sessionKey
					,	p_module:		'iud_u_rec_sleep_record_note'
					,	p_action:		Ext.String.htmlEncode(e.value)
					,	p_id:			e.record.data.PK
				}
				, success: SleepTracker.util.onAjaxResp
				, failure: SleepTracker.util.onAjaxResp
				, scope: {
					handler: function(obj){
						var store = Ext.getStore('SleepRecords');
						var rows = store.getRange();
						Ext.each(rows, function(row) {
							if(row.data.PK === e.record.data.PK){
							  var idx = row.index;
							  store.getAt(idx).commit();
							}
						});
					}
				  }
			});
		}
	  }
	, initComponent: function() {
		console.log('SleepRecordGrid()'); 
		this.callParent();
	  }
});
